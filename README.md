Летняя школа 2018
=================
Дата проведения: 14-30 июня.

14 июня открытие:
1. Квиз

30 июня закрытие:
1. Защита портфолио
2. Вручение дипломов

Планерки проводятся каждую неделю, по средам.
Следующая планерка 16 мая в 15:30.

Список курсов:
-----------

- [Студия мультипликации 11+](https://gitlab.com/fsir_lab/summer_school/blob/master/animation/description.md) (Юлия Курмазова);
- [Каллиграфия и леттеринг 11+](https://gitlab.com/fsir_lab/summer_school/blob/master/lettering:calligraphy/description.md) (Галина Зорина);
- [Графика. Книжные иллюстрации 11+](https://gitlab.com/fsir_lab/summer_school/blob/master/book_illustrations/description.md) (Александра Куставинова);
- [Фотография 10+](https://gitlab.com/fsir_lab/summer_school/blob/master/photograph/description.md) (Пестова Юлия);
- [Minecraft 1: программируй свой мир 14+](https://gitlab.com/fsir_lab/summer_school/blob/master/minecraft_1/description.md) (Зоя Воловикова);
- [Minecraft 2: создание модов 14+](https://gitlab.com/fsir_lab/summer_school/blob/master/minecraft_2/description.md) (Михаил Рыбин, Михаил Мустакимов);
- [Умные вещи: робототехника на платформе Arduino 11+](https://gitlab.com/fsir_lab/summer_school/blob/master/arduino/description.md) (Валерия Юринская). 
- [Основы 3D моделирования. Работа с 3D принтерами 14+](https://gitlab.com/fsir_lab/summer_school/blob/master/3d_modeling/description.md) (Денис Ромицын);
- [Аниматроники: роботы для начинающих 11+](https://gitlab.com/fsir_lab/summer_school/blob/master/animatronics/description.md) ?;
- [Робототехника на основе конструктора HUNA-MRT 9](https://gitlab.com/fsir_lab/summer_school/blob/master/constructor_HUNA-MRT/description.md) ?;
- [Программирование для детей 7+](https://gitlab.com/fsir_lab/summer_school/blob/master/programming_for_kids/description.md) (Анна Ромме);
- [Программирование мобильных устройств для начинающих 14+](https://gitlab.com/fsir_lab/summer_school/blob/master/android/description.md) (Михаил Мустакимов);

Что делаем?
-----------
- Преподаватели расписывают программу на 10 дней по 4 часа.
- Преподавателям также нужно предусмотреть урезание курса до 3 мастер-классов.
- Прописать задание на квест.
- Оформить буклет.
- Оформить афишу.
- Сделать презентацию.

**Юринская:**
1. Координирует

**Ромме:**
1. Создать контент (текст для поста в группе, объявления)

**Ромицын:**
1. Покупка сладостей/воды/сока
2. Поиск столовой/кафе для доставки еды

**Куставинова:**

*Занимается оформлением*
1. Буклет
2. Презентация

**Стерхов:**
1. Лендинг
2. Организация работы аниматоров

**Пестова:**
1. Придумать форму буклета: [Cмотри файл booklet.md](https://gitlab.com/fsir_lab/summer_school/blob/master/booklet.md)
2. Поиск каналов распространения (набор, наглядная агитация) - ИАЗ, 12шк.

**Воловикова:**
1. Группа ВК

**Мустакимов:**
1. Проверка и установка ПО

**?Екатерина?:**
1. Найти спонсоров


---------------------------------------------

*Попросить Рыбина создать дизайн для оформления*

