Варианты формы буклета
==

1. Гексафлексагон
    <p><a href="https://youtu.be/CqlXyUxYC4s" target="Youtube">Инструкция по сборке</a></p>
    <img src="https://i.ytimg.com/vi/CqlXyUxYC4s/maxresdefault.jpg" width="700"/>
    <p>Шаблон:</p>
    <img src="https://multiurok.ru/img/307064/image_59774f90ec1ff.jpg" width="500"/>
2. Просто, но оригинально 
    <p><img src="https://www.pressfoto.ru/blog/wp-content/uploads/2014/09/How-to-design-cool-brochure-17.jpg" width="700"/></p>
3. Объемный гексафлексагон (очень сложный)
    <p><a href="https://youtu.be/CqlXyUxYC4s" target="Youtube">Инструкция по сборке</a></p>
    <img src="http://www.infovia.net/cdn/6/2011/799/paper-flexagon-template_264714.jpg" width="700"/>
    <p>Шаблоны:</p>
    <img src="https://lh3.googleusercontent.com/-OdE4HrZvQpM/WA4u6r7WiWI/AAAAAAAAMS0/1hcrMASDmmsE_8lLqurQa8xnv726xuB1wCJoC/w530-h409-n-rw/%25D0%25A4%25D0%259B%25D0%2595%25D0%259A%25D0%25A1%25D0%2590%25D0%2593%25D0%259E%25D0%259D.png" width="700"/>
    <img src="http://www.alcaponeblog.com/wp-content/uploads/2017/12/template-latest-hexaflexagon-template-hexaflexagon-template-pictures.jpg" width="700"/>